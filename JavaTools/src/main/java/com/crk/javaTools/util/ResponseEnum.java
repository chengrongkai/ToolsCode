package com.crk.javaTools.util;

/**
 * Description:
 * 响应状态码
 * @author:
 * Date: 2019-12-17
 * Time: 14:52
 */
public enum ResponseEnum {

    SUCCESS(200,"处理成功"),
    FAIL(500,"系统异常"),
    PARAM_ERROR(501,"参数有误"),
    USERCHECK_ERROR(502,"用户名或密码有误"),
    USEREXIST_ERROR(503,"用户名或手机号已存在"),
    REGISTER_ERROR(504,"注册失败"),
    DATAEXIST_ERROR(504,"数据已存在,不允许重复添加"),
    CHILDEXIST_ERROR(505,"此部门下面还有子部门,请确定删除所有的子部门后再进行此操作"),
    OLDPWD_ERROR(506,"原密码错误"),
    UNNORMAL_ERROR(600,"非法操作，您无此功能操作权限"),
    ROLE_CONFIG_ERROR(1001,"管理员尚未配置此类型用户对应的角色，请联系管理员"),
    OLD_PASSWORD_ERROR(1002,"原始密码错误"),
    IDCARD_ERROR(1003,"身份证号不允许为空"),
    IDCARD_CONFIG_ERROR(1004,"系统尚未录入您的信息，请联系管理员"),
    ;
    /**
     * 状态码
     */
    private int code;
    /**
     * 响应消息
     */
    private String msg;

    ResponseEnum(int code,String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
