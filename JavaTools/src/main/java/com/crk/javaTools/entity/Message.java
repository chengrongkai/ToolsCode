package com.crk.javaTools.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Description:
 *
 * @author: chengrongkai
 * Date: 2020-07-23
 * Time: 15:31
 */
@Data
public class Message implements Serializable {
    private static final long serialVersionUID = 1L;

    private String sender;

    private String message;

    private Date date;
}
