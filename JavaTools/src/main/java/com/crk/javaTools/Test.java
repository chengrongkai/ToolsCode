package com.crk.javaTools;


import com.aspose.words.Document;
import com.aspose.words.License;
import com.aspose.words.SaveFormat;


import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * Description:
 *
 * @author: chengrongkai
 * Date: 2020/12/9
 */
public class Test {

    /**
     * 读取证书
     * @return
     */
    private static boolean getLicense() {
        boolean result = false;
        try {

            InputStream is = Test.class.getClassLoader().getResourceAsStream("license.xml"); // license.xml应放在..\WebRoot\WEB-INF\classes路径下
            License aposeLic = new License();
            aposeLic.setLicense(is);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void pdf2doc(String wordPath, String pdfPath){
        if (!getLicense()) { // 验证License 若不验证则转化出的pdf文档会有水印产生
            return;
        }
        try {
            long old = System.currentTimeMillis();
            File file = new File(pdfPath); //新建一个pdf文档
            FileOutputStream os = new FileOutputStream(file);
            Document doc = new Document(wordPath); //Address是将要被转化的word文档
            doc.save(os, SaveFormat.DOC);//全面支持DOC, DOCX, OOXML, RTF HTML, OpenDocument, PDF, EPUB, XPS, SWF 相互转换
            long now = System.currentTimeMillis();
            os.close();
            System.out.println("共耗时：" + ((now - old) / 1000.0) + "秒"); //转化用时
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        String doc2pdf = "D:\\test1.doc";
        String pdfPath = "D:\\test1.pdf";
        pdf2doc(pdfPath,doc2pdf);
    }
}
