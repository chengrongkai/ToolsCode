package com.crk.javaTools;

import com.aliyun.oss.ClientConfiguration;
import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.common.auth.CredentialsProvider;
import com.aliyun.oss.common.auth.DefaultCredentialProvider;
import com.aliyun.oss.common.auth.DefaultCredentials;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectRequest;
import com.crk.javaTools.config.OssConfig;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.InputStream;

/**
 * oss存储工具
 * @author chengrongkai
 * @description:
 * @date 2020/7/9
 */
public class OssTools {
    @Autowired
    private static OssConfig ossConfig;

    private static OSSClient ossClient;

    static {
        DefaultCredentials defaultCredentials = new DefaultCredentials(ossConfig.getAccessKeyId(), ossConfig.getAccessKeySecret());
        DefaultCredentialProvider credentialsProvider = new DefaultCredentialProvider(defaultCredentials);
        ClientConfiguration conf = new ClientConfiguration();
        // 设置HTTP最大连接数为10
        conf.setMaxConnections(ossConfig.getMaxConnections());
        // 设置TCP连接超时为5000毫秒
        conf.setConnectionTimeout(ossConfig.getConnectionTimeout());
        // 设置最大的重试次数为3
        conf.setMaxErrorRetry(ossConfig.getMaxErrorRetry());
        // 设置Socket传输数据超时的时间为2000毫秒
        conf.setSocketTimeout(ossConfig.getSocketTimeout());
        ossClient = new OSSClient(ossConfig.getEndpoint(),credentialsProvider,conf);
    }

    public String uploadFile(String path, String fileName, InputStream file) {
        String fileType = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length()).toLowerCase();
        return putObject(path, file, fileType, fileName);
    }

    /**
     * @param file
     * @param fileType
     * @param fileName
     * @return String
     * @MethodName: putObject
     * @Description: 上传文件
     */
    private String putObject(String path, InputStream file, String fileType, String fileName) {
        String url = null;      //默认null
        try {
            InputStream input = file;
            ObjectMetadata meta = new ObjectMetadata();             // 创建上传Object的Metadata
            meta.setContentType(contentType(fileType));       // 设置上传内容类型
            meta.setCacheControl("no-cache");                   // 被下载时网页的缓存行为
//            PutObjectRequest request = new PutObjectRequest(ossConfig.getBucketName(), "files/"+path.split(sysConfig.getRootPath())[1].toString()+fileName, input, meta);           //创建上传请求
//            ossClient.putObject(request);
//            url = config.getEndpoint().replaceFirst("http://", "http://" + config.getBucketName() + ".") + "/" + fileName;
        } catch (OSSException oe) {
            oe.printStackTrace();
            return null;
        } catch (ClientException ce) {
            ce.printStackTrace();
            return null;
        } finally {
            ossClient.shutdown();
        }
        return url;
    }

    /**
     * @param fileType
     * @return String
     * @MethodName: contentType
     * @Description: 获取文件类型
     */
    private static String contentType(String fileType) {
        String contentType = "";
        if ("bmp".equalsIgnoreCase(fileType)){
            contentType = "image/bmp";
        }else if ("gif".equalsIgnoreCase(fileType)){
            contentType = "image/gif";
        }else if ("jpeg".equalsIgnoreCase(fileType)||"png".equalsIgnoreCase(fileType)||"jpg".equalsIgnoreCase(fileType)){
            contentType = "image/jpeg";
        }else if ("html".equalsIgnoreCase(fileType)){
            contentType = "text/html";
        }else if ("txt".equalsIgnoreCase(fileType)){
            contentType = "text/plain";
        }else if ("vsd".equalsIgnoreCase(fileType)){
            contentType = "application/vnd.visio";
        }else if ("ppt".equalsIgnoreCase(fileType)||"pptx".equalsIgnoreCase(fileType)){
            contentType = "application/vnd.ms-powerpoint";
        }else if ("doc".equalsIgnoreCase(fileType)||"docx".equalsIgnoreCase(fileType)){
            contentType = "application/msword";
        }else if ("xml".equalsIgnoreCase(fileType)){
            contentType = "text/xml";
        }else if ("mp4".equalsIgnoreCase(fileType)){
            contentType = "video/mp4";
        }else {
            contentType = "application/octet-stream";
        }
        return contentType;
    }


}
