package com.crk.javaTools.controller;

import com.crk.javaTools.FileTools;
import com.crk.javaTools.PdfTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;

/**
 * Description:
 *
 * @author: chengrongkai
 * Date: 2020/12/7
 */
@Controller
@RequestMapping("pdf")
public class PdfController {

    @Autowired
    HttpServletRequest request;


    @RequestMapping("convertToImageSimple")
    public void convertToImage(@RequestParam("file") MultipartFile file,boolean multiple,HttpServletResponse response){
        InputStream inputStream = null;
        OutputStream  outputStream = null;
        try {
            inputStream = file.getInputStream();
            outputStream = response.getOutputStream();
            setResponse(file.getOriginalFilename(),multiple,response);
            PdfTools.pdfToImage(inputStream,outputStream,multiple);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (null != inputStream){
                try {
                    inputStream.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            if (null != outputStream){
                try {
                    outputStream.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

    }

    /**
     * PDF转图片
     * @param fileName PDF文件名
     * @param multiple 是否按页转成多张
     */
    @RequestMapping("convertToImage")
    public void convertToImage(@RequestParam("fileName") String fileName,boolean multiple,HttpServletResponse response){
        OutputStream  outputStream = null;
        try {
            String filePath = FileTools.getUploadPath()+fileName;
            outputStream = response.getOutputStream();
            setResponse(fileName,multiple,response);
            PdfTools.pdfToImage(filePath,outputStream,multiple);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (null != outputStream){
                try {
                    outputStream.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

        }

    }

    private void setResponse(String fileName,boolean multiple,HttpServletResponse response){
        if (multiple){
            fileName = fileName.substring(0,fileName.lastIndexOf(".")+1)+"zip";
        }else{
            fileName = fileName.substring(0,fileName.lastIndexOf(".")+1)+"png";
        }
        try {
            response.setContentType("application/octet-stream");
            response.setHeader("Accept-Ranges", "bytes");
            response.setHeader("Content-disposition",
                    "attachment; filename=" + URLEncoder.encode(fileName, "UTF-8"));
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    @GetMapping("page")
    public ModelAndView pdfPage(){
        ModelAndView modelAndView = new ModelAndView("pdf.html");
        return modelAndView;
    }
}
